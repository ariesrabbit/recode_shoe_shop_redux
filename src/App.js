import logo from "./logo.svg";
import "./App.css";
import Shoe_Redux from "./Shoe_Shop_Redux/Shoe_Redux";

function App() {
  return (
    <div className="App">
      <Shoe_Redux />
    </div>
  );
}

export default App;
