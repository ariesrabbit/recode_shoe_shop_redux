import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux"; //thêm dòng này
import { createStore } from "redux"; // thêm dòng này
import { rootReducer_Shoe_shop } from "./Shoe_Shop_Redux/redux/reducer/rootReducer";
const root = ReactDOM.createRoot(document.getElementById("root"));

const store = createStore(
  rootReducer_Shoe_shop,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
