import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QUATITY } from "./redux/constant/shoeConstant";

class GioHang extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}$</td>
          <td>
            <img style={{ width: 100 }} src={item.image} alt="" />
          </td>
          <td>
            {/* nút tăng số lượng */}
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.handleChangeQuatity(index, true);
              }}
            >
              +
            </button>
            <span className="px-5">{item.soLuong}</span>
            {/* nút giảm số lượng */}
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleChangeQuatity(index, false);
              }}
            >
              -
            </button>
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleDeleteCart(index);
              }}
            >
              <i className="fa fa-trash" />
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Tên</th>
              <th>Giá</th>
              <th>Hình ảnh</th>
              <th>Số lượng</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleChangeQuatity: (index, value) => {
      dispatch({
        type: CHANGE_QUATITY,
        payload: { index, value },
      });
    },
  };
};
export default connect(null, mapDispatchToProps)(GioHang);
