import { combineReducers } from "redux";
import { shoeReducer } from "./shoeReducer";

export let rootReducer_Shoe_shop = combineReducers({ shoeReducer });
