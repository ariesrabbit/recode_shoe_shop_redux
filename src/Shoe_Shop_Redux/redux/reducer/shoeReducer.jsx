import { data_shoes } from "../../dataShoe";
import {
  ADD_TO_CART,
  DELETE_CART,
  CHANGE_QUATITY,
} from "./../constant/shoeConstant";

let initialState = {
  shoes: data_shoes,
  gioHang: [],
};

export let shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      // kiểm tra giỏ hàng hiện tại có sản phẩm hay chưa
      let index = state.gioHang.findIndex((item) => {
        return item.id == payload.id;
      });

      let cloneGioHang = [...state.gioHang];

      if (index == -1) {
        let newSp = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSp);
      } else {
        cloneGioHang[index].soLuong++;
      }

      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case DELETE_CART: {
      let cloneGioHang = [...state.gioHang];
      // xóa giỏ hàng dựa vào key index
      cloneGioHang.splice(payload, 1);
      state.gioHang = cloneGioHang;
      return { ...state };
    }

    case CHANGE_QUATITY: {
      const { index, value } = payload;
      let cloneGioHang = [...state.gioHang];
      if (value) {
        cloneGioHang[index].soLuong++;
      } else {
        if (cloneGioHang[index].soLuong <= 1) {
          cloneGioHang.splice(index, 1);
        } else cloneGioHang[index].soLuong--;
      }

      state.gioHang = cloneGioHang;
      return { ...state };
    }

    default:
      return state;
  }
};
