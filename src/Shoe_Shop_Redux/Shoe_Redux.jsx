import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
import { connect } from "react-redux";
import GioHang from "./GioHang";
import { ADD_TO_CART, DELETE_CART } from "./redux/constant/shoeConstant";

class Shoe_Redux extends Component {
  renderContent = () => {
    return this.props.shoes.map((item, index) => {
      return (
        <ItemShoe
          key={index}
          data={item}
          handleAddToCart={this.props.handleAddToCart}
        />
      );
    });
  };
  render() {
    return (
      <div className="container py-5">
        <GioHang
          cart={this.props.cart}
          handleDeleteCart={this.props.handleDeleteCart}
        />
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    shoes: state.shoeReducer.shoes,
    cart: state.shoeReducer.gioHang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },
    handleDeleteCart: (index) => {
      dispatch({
        type: DELETE_CART,
        payload: index,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Shoe_Redux);
